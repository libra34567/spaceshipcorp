﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class BattleState : GameState
{
    [SerializeField]
    private GameObject BattleCanvas;
    //hp bar control

    Rewired.Player rewiredPlayer0;
    Rewired.Player rewiredPlayer1;

    Rewired.Player rewiredPlayer2;
    Rewired.Player rewiredPlayer3;


    public Transform team1SpawnPos;
    public Transform team2SpawnPos;
     
    private SpaceShip Team1Ship;
    private SpaceShip Team2Ship;

    void OnEnable(){
        BattleCanvas.SetActive(true);

        Team1Ship = (Instantiate(ManagersManager.Inst.ShipGetter.GetSelectedShip(1).gameObject, team1SpawnPos.position, Quaternion.identity, team1SpawnPos)).GetComponent<SpaceShip>();
        Team1Ship.TeamId = 1;
        Team2Ship = Instantiate(ManagersManager.Inst.ShipGetter.GetSelectedShip(2).gameObject, team2SpawnPos.position, Quaternion.identity, team2SpawnPos).GetComponent<SpaceShip>();
        Team2Ship.TeamId = 2;
    }

    void OnDisable(){
        BattleCanvas.SetActive(false);

        Destroy(Team1Ship.gameObject);
        Destroy(Team2Ship.gameObject);
    }

    void Update(){
        // if (Team1Ship.Hp < 0 || Team2Ship.Hp < 0){
        //     if (Team1Ship.Hp < 0){
        //         //team 2 win
        //     }else{
        //         //Team 1 win
        //     }

        //     //end game
        //     ManagersManager.Inst.GameStateManager.Push<MenuState>();
        // }


    }
}

