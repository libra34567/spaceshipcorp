﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour
{
    public int MaxHp = 100;
    [HideInInspector]
    public int Hp;
    public int TeamId;//1 or 2

    public Transform[] SpawnPos;

    public float movementFactor;
    
    public Transform[] playerPrefabs;

    public Player player0;
    public Player player1;

    public Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        if (ManagersManager.Inst.GameStateManager.curState.GetType() == typeof(BattleState)){
            Hp = MaxHp;
            //spawn players
            player0 = Instantiate(playerPrefabs[0].gameObject, SpawnPos[0].position, Quaternion.identity, transform).GetComponent<Player>();
            player1 = Instantiate(playerPrefabs[0].gameObject, SpawnPos[1].position, Quaternion.identity, transform).GetComponent<Player>();
            player0.Init(TeamId, 0);
            player1.Init(TeamId, 1);
            rb = this.GetComponent<Rigidbody>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (ManagersManager.Inst.GameStateManager.curState.GetType() == typeof(BattleState)){
            //battle state

        }
    }


    public void MoveUp(){
        rb.MovePosition(this.transform.position + Vector3.up * movementFactor * Time.deltaTime);
    }
    public void MoveDown(){
        rb.MovePosition(this.transform.position + Vector3.down * movementFactor * Time.deltaTime);
    }
    public void MoveLeft(){
        rb.MovePosition(this.transform.position + Vector3.left * movementFactor * Time.deltaTime);
    }
    public void MoveRight(){
        rb.MovePosition(this.transform.position + Vector3.right * movementFactor * Time.deltaTime);
    }

}
