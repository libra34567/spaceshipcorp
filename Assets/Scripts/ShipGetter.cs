﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipGetter : MonoBehaviour
{
    public SpaceShip[] listOfShips;

    public SpaceShip GetShip(int shipIndex) => listOfShips[shipIndex];


    public SpaceShip GetSelectedShip(int teamId){
        if (teamId == 1){
            return GetShip(shipIndexTeam1);
        }else if (teamId == 2){
            return GetShip(shipIndexTeam2);
        }else{
            Debug.LogWarning("wtf ship index");
            return null;
        }
    }
    
    
    public int shipIndexTeam1 = 0;
    public int shipIndexTeam2 = 0;

    [HideInInspector]
    public int prevShipIndexTeam1 = 0;
    [HideInInspector]
    public int prevShipIndexTeam2 = 0;

    
}
