﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configs
{
    public static string MovementX = "MovementX";
    public static string MovementY = "MovementY";
    public static string Confirm = "Confirm";
    public static string Increment = "Increment";
    public static string Decrement = "Decrement";
    
    public static string Jump = "Jump";
}
