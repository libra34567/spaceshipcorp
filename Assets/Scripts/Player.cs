﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class Player : MonoBehaviour
{

    [SerializeField]
    private float moveForceFactor;
    [SerializeField]
    private float ladderMoveFactor;
    [SerializeField]
    private float jumpForce;

    public int controlerId = -1;
    private Rigidbody rb;
    private Rewired.Player rewiredPlayer = null;

    public float jumpCD;
    public float lastJumpTime;
    public System.Action triggerConfirmAssigner;

    public bool OnLadder = false;

    // Start is called before the first frame update
    public void Init(int teamId, int inShipId){
        if (teamId == 1){
            if (inShipId == 0){
                controlerId = 0;
            }else if (inShipId == 1){
                controlerId = 1;
            }
        }else if (teamId == 2){
            if (inShipId == 0){
                controlerId = 2;
            }else if (inShipId == 1){
                controlerId = 3;
            }
        }

        rewiredPlayer = ReInput.players.GetPlayer(controlerId);
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rewiredPlayer == null){
            return;
        }
        //can jumpoff ladder
        if (OnLadder){
            Debug.Log(1);
            rb.useGravity = false;
            rb.isKinematic = true;
            Vector3 movePosRight = Vector3.right * rewiredPlayer.GetAxis(Configs.MovementX) * ladderMoveFactor * Time.deltaTime;
            Vector3 movePosUp = Vector3.up * rewiredPlayer.GetAxis(Configs.MovementY) * ladderMoveFactor * Time.deltaTime;
            Vector3 newMoveTargetPos = transform.position + movePosRight + movePosUp;
            //Normalized????? as he will move faster when diagnal
            rb.MovePosition(newMoveTargetPos);
            // rb.AddForce(Vector3.right * rewiredPlayer.GetAxis(Configs.MovementX) * moveForceFactor, ForceMode.Acceleration);
            // rb.AddForce(Vector3.up * rewiredPlayer.GetAxis(Configs.MovementY) * moveForceFactor, ForceMode.Acceleration);
        }else{
            //todo:
            //if previously on ladder, then turn on gravity
            rb.useGravity = true;
            rb.isKinematic = false;

            rb.AddForce(Vector3.right * rewiredPlayer.GetAxis(Configs.MovementX) * moveForceFactor, ForceMode.Force);
            if (rewiredPlayer.GetButtonDown(Configs.Jump)){
                if (Time.time - lastJumpTime > jumpCD){
                    //you can jump
                    lastJumpTime = Time.time;
                    rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                }
            }
        }


        //listener trigger
        // if (rewiredPlayer.GetButtonDown(Configs.Confirm)){
        //     triggerConfirmAssigner.Invoke();
        // }
    }

}
