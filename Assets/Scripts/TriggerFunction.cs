﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TriggerType{
    None,
    Ladder, 
    ShipMoveUp, 
    ShipMoveDown, 
    ShipMoveLeft, 
    ShipMoveRight
}
public class TriggerFunction : MonoBehaviour
{
    private TargetOfTrigger targetOfTrigger;

    private Player player;
    
    private SpaceShip ship;

    private Player ConfirmedPlayer;

    [SerializeField]
    private TriggerType triggerType;

    void OnTriggerEnter(Collider c){
        //show confirm button
        //get player
        player = c.gameObject.GetComponent<Player>();  
        ship = this.GetComponentInParent<SpaceShip>();
        switch (triggerType){
            case TriggerType.Ladder:
                player.OnLadder = true;
                break;
        }
    }

//keep ship moveing when player confirmed
    void OnTriggerStay(Collider c){
        if (ConfirmedPlayer != null){
            //move in that dir
            switch (triggerType){
                case TriggerType.ShipMoveUp:
                    ship.MoveUp();
                    break;
                case TriggerType.ShipMoveDown:
                    ship.MoveDown();
                    break;
                case TriggerType.ShipMoveLeft:
                    ship.MoveLeft();
                    break;
                case TriggerType.ShipMoveRight:
                    ship.MoveRight();
                    break;
            }
        }
    }

    void OnTriggerExit(Collider c){
        switch (triggerType){
            case TriggerType.Ladder:
                player.OnLadder = false;
                break;
        }
    }

}
