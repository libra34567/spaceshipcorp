﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;


public class MenuState : GameState
{
    [SerializeField]
    private GameObject MenuCanvas;

    Rewired.Player rewiredPlayer0;
    Rewired.Player rewiredPlayer1;

    Rewired.Player rewiredPlayer2;
    Rewired.Player rewiredPlayer3;

    private ShipGetter _shipGetter;


    [SerializeField]
    private Transform ShipDisplayPointTeam1;

    [SerializeField]
    private Transform ShipDisplayPointTeam2;



    private void Start(){
        
        rewiredPlayer0 = ReInput.players.GetPlayer(0);
        rewiredPlayer1 = ReInput.players.GetPlayer(1);
        rewiredPlayer2 = ReInput.players.GetPlayer(2);
        rewiredPlayer3 = ReInput.players.GetPlayer(3);

        _shipGetter = ManagersManager.Inst.ShipGetter;
    }

    void OnEnable(){
        
        MenuCanvas.SetActive(true);

        // var CamraCtrl = Camera.main.GetComponent<CameraController>();
        // CamraCtrl.OnMainMenu();
        
        // var audioController = Camera.main.transform.parent.GetComponent<AudioController>();
        // audioController.PlayTitleMusic();
    }

    void OnDisable(){
        MenuCanvas.SetActive(false);
    }


    private void Update(){
        if (rewiredPlayer0.GetButtonDown(Configs.Confirm)){// || rewiredPlayer1.GetButtonDown(Configs.Confirm) || rewiredPlayer2.GetButtonDown(Configs.Confirm) || rewiredPlayer3.GetButtonDown(Configs.Confirm)){
            //Start the game
            ManagersManager.Inst.GameStateManager.Push<BattleState>();
            return;
        }


        //Team 1 increment
        if (rewiredPlayer0.GetButtonDown(Configs.Increment)){
            //next ship for Team 1
            _shipGetter.shipIndexTeam1++;
            if (_shipGetter.shipIndexTeam1 > _shipGetter.listOfShips.Length - 1){
                _shipGetter.shipIndexTeam1 = 0;
            }
        }
        //team 1 decrement
        if (rewiredPlayer0.GetButtonDown(Configs.Decrement)){
            _shipGetter.shipIndexTeam1--;
            if (_shipGetter.shipIndexTeam1 < 0){
                _shipGetter.shipIndexTeam1 = _shipGetter.listOfShips.Length - 1;
            }
        }

        //TODO: team2 increment and decrement






        //team 1 showing ship

        if (_shipGetter.prevShipIndexTeam1 != _shipGetter.shipIndexTeam1 || ShipDisplayPointTeam1.childCount == 0){

            //Destroy all, and spawn current
            foreach (Transform child in ShipDisplayPointTeam1){
                Destroy(child.gameObject);
            }

            Instantiate(_shipGetter.GetSelectedShip(1), ShipDisplayPointTeam1.position, Quaternion.identity, ShipDisplayPointTeam1);
        }

        if (_shipGetter.prevShipIndexTeam2 != _shipGetter.shipIndexTeam2 || ShipDisplayPointTeam2.childCount == 0){

            //Destroy all, and spawn current
            foreach (Transform child in ShipDisplayPointTeam2){
                Destroy(child.gameObject);
            }

            Instantiate(_shipGetter.GetSelectedShip(2), ShipDisplayPointTeam2.position, Quaternion.identity, ShipDisplayPointTeam2);
        }

        



        // if (rewiredPlayer0.GetAnyButtonDown() || rewiredPlayer1.GetAnyButtonDown()){
        //     ManagerManager.Inst.GameStateManager.Push<BattleState>();
        //     Camera.main.transform.parent.GetComponent<AudioController>().PlayStartGameButtonAudio();
        // }
    
        _shipGetter.prevShipIndexTeam1 = _shipGetter.shipIndexTeam1;
        _shipGetter.prevShipIndexTeam2 = _shipGetter.shipIndexTeam2;
    }

}
