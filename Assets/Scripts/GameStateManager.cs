﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateManager : MonoBehaviour
{

    public GameState curState {
        get {
            return stateStack.Peek();
        }
    }

    private Dictionary<Type, GameState> typeToState = new Dictionary<Type, GameState>();
    private Stack<GameState> stateStack = new Stack<GameState>();
    

    public void Init()
    {
        GameState[] states = GetComponentsInChildren<GameState>(true);
        foreach (GameState state in states)
        {
            state.gameObject.SetActive(false);

            typeToState.Add(state.GetType(), state);
        }

        Push(typeof(MenuState));
    }

    public T Push<T>() where T : GameState
    {
        GameState newState = Push(typeof(T));
        return (T)newState;
    }
    public GameState Push(Type stateType)
    {
        if (stateStack.Count > 0)
        {
            foreach (GameState gameState in stateStack)
            {
                if (gameState.gameObject.activeSelf)
                {
                    gameState.gameObject.SetActive(false);
                }
            }
        }

        GameState newState = typeToState[stateType];
        newState.gameObject.SetActive(true);
        stateStack.Push(newState);
        return newState;
    }


    public void Pop()
    {
        GameState topState = stateStack.Pop();
        topState.gameObject.SetActive(false);

        // if (stateStack.Peek() is OverlayUI)
        // {
        //     Type overlayScreenType = screenStack.Pop().GetType();
        //     screenStack.Peek().gameObject.SetActive(true);
        //     OverlayPush(overlayScreenType);
        // }
        // else
        // {
            stateStack.Peek().gameObject.SetActive(true);
        // }

    }
}

