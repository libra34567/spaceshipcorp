﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagersManager : MonoBehaviour
{
    public static ManagersManager Inst;

    public GameStateManager GameStateManager;
    public ShipGetter ShipGetter;

    void Awake(){
        GameStateManager = this.GetComponent<GameStateManager>();
        GameStateManager.Init();

        ShipGetter = this.GetComponent<ShipGetter>();

        
        Inst = this;
    }

}
